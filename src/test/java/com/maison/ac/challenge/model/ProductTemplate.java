package com.maison.ac.challenge.model;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

public class ProductTemplate implements TemplateLoader {

    public static final String VALID = "valid";
    public static final String ONE = "one";
    public static final String WITH_PARENT = "withParent";

    @Override
    public void load() {
        Fixture.of(Product.class).addTemplate(VALID, new Rule(){{
            add("name", regex("Phone [0-9a-f]{5}"));
            add("description", regex("\\w{10,20}"));
        }});

        Fixture.of(Product.class).addTemplate(ONE, new Rule(){{
            add("id", 1L);
        }});

        Fixture.of(Product.class).addTemplate(WITH_PARENT).inherits(VALID, new Rule(){{
            add("parent", one(Product.class, ONE));
        }});

        Fixture.of(Image.class).addTemplate(VALID, new Rule(){{
            add("type", uniqueRandom("image/bmp", "image/gif", "image/jpeg", "image/png", "image/tiff"));
        }});
    }

}

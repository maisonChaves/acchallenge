package com.maison.ac.challenge.controller;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.maison.ac.challenge.model.Product;
import com.maison.ac.challenge.model.ProductTemplate;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProductControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeClass
    public static void setUp() {
        FixtureFactoryLoader.loadTemplates("com.maison.ac.challenge.model");
    }

    @Test
    public void list() throws Exception {
        ResponseEntity<Product[]> entity = this.restTemplate.getForEntity("/product", Product[].class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().length).isGreaterThan(Collections.EMPTY_LIST.size());
    }

    @Test
    public void listComplete() throws Exception {
        ResponseEntity<Product[]> entity = this.restTemplate.getForEntity("/product/complete", Product[].class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().length).isGreaterThan(Collections.EMPTY_LIST.size());
    }

    @Test
    public void get() throws Exception {
        ResponseEntity<Product> entity = this.restTemplate.getForEntity("/product/1", Product.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody()).isNotNull();
    }

    @Test
    public void getComplete() throws Exception {
        ResponseEntity<Product> entity = this.restTemplate.getForEntity("/product/1/complete", Product.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody()).isNotNull();
    }

    @Test
    public void create() throws Exception {

        Product product = Fixture.from(Product.class).gimme(ProductTemplate.VALID);

        ResponseEntity<Product> entity = this.restTemplate.postForEntity("/product", product, Product.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(entity.getHeaders().getLocation()).isNotNull();
    }

    @Test
    public void createParent() throws Exception {

        Product product = Fixture.from(Product.class).gimme(ProductTemplate.VALID);
        Product one = Fixture.from(Product.class).gimme(ProductTemplate.ONE);
        product.setParent(one);

        ResponseEntity<Product> entity = this.restTemplate.postForEntity("/product", product, Product.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(entity.getHeaders().getLocation()).isNotNull();
    }

    @Test
    public void update() throws Exception {

        Product newProduct = Fixture.from(Product.class).gimme(ProductTemplate.VALID);
        ResponseEntity<Product> entity = this.restTemplate.postForEntity("/product", newProduct, Product.class);
        Long id = idFromLocation(entity);

        Product product = Fixture.from(Product.class).gimme(ProductTemplate.VALID);
        HttpEntity<Product> requestEntity = new HttpEntity<>(product);
        ResponseEntity<Product> response = this.restTemplate.exchange("/product/{id}", HttpMethod.PUT, requestEntity, Product.class, id);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getName()).isEqualTo(product.getName());
        assertThat(response.getBody().getName()).isNotEqualTo(newProduct.getName());
    }

    @Test
    public void delete() throws Exception {
        Product product = Fixture.from(Product.class).gimme(ProductTemplate.VALID);
        ResponseEntity<Product> entity = this.restTemplate.postForEntity("/product", product, Product.class);
        Long id = idFromLocation(entity);

        ResponseEntity<Void> response = this.restTemplate.exchange("/product/{id}", HttpMethod.DELETE, null, Void.class, id);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
    }

    @Test
    public void listChildren() throws Exception {

        Product product = Fixture.from(Product.class).gimme(ProductTemplate.VALID);
        ResponseEntity<Product> entity = this.restTemplate.postForEntity("/product", product, Product.class);
        Long id = idFromLocation(entity);

        Product child = Fixture.from(Product.class).gimme(ProductTemplate.VALID);
        Product one = new Product();
        one.setId(id);
        child.setParent(one);
        this.restTemplate.postForEntity("/product", child, Product.class);

        ResponseEntity<Product[]> response = this.restTemplate.getForEntity("/product/{id}/products", Product[].class, id);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().length).isGreaterThan(Collections.EMPTY_LIST.size());
    }

    private Long idFromLocation(ResponseEntity<Product> entity) {
        String[] fragment = entity.getHeaders().getLocation().getPath().split("/");
        return Long.parseLong(fragment[2]);
    }

}
package com.maison.ac.challenge.controller;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.maison.ac.challenge.model.Image;
import com.maison.ac.challenge.model.Product;
import com.maison.ac.challenge.model.ProductTemplate;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ImageControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private Long productId;

    @BeforeClass
    public static void setUpClass() {
        FixtureFactoryLoader.loadTemplates("com.maison.ac.challenge.model");
    }

    @Before
    public void setUp() throws Exception {
        if(productId == null){
            this.productId = newProduct();
        }
    }

    @Test
    public void list() throws Exception {

        Image image = Fixture.from(Image.class).gimme(ProductTemplate.VALID);
        this.restTemplate.postForEntity("/product/{id}/image", image, Image.class, productId);

        ResponseEntity<Image[]> entity = this.restTemplate.getForEntity("/product/{id}/image", Image[].class, productId);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().length).isGreaterThan(Collections.EMPTY_LIST.size());
    }

    @Test
    public void get() throws Exception {
        Image newImage = Fixture.from(Image.class).gimme(ProductTemplate.VALID);
        ResponseEntity<Image> entity = this.restTemplate.postForEntity("/product/{id}/image", newImage, Image.class, productId);
        Long id = idFromLocation(entity);

        ResponseEntity<Image> response = this.restTemplate.getForEntity("/product/{id}/image/{id}", Image.class, productId, id);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    public void create() throws Exception {
        Image image = Fixture.from(Image.class).gimme(ProductTemplate.VALID);

        ResponseEntity<Image> entity = this.restTemplate.postForEntity("/product/{id}/image", image, Image.class, productId);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(entity.getHeaders().getLocation()).isNotNull();
    }

    @Test
    public void update() throws Exception {
        Image newImage = Fixture.from(Image.class).gimme(ProductTemplate.VALID);
        ResponseEntity<Image> entity = this.restTemplate.postForEntity("/product/{id}/image", newImage, Image.class, productId);
        Long id = idFromLocation(entity);

        Image product = Fixture.from(Image.class).gimme(ProductTemplate.VALID);
        HttpEntity<Image> requestEntity = new HttpEntity<>(product);
        ResponseEntity<Image> response = this.restTemplate.exchange("/product/{id}/image/{id}", HttpMethod.PUT, requestEntity, Image.class, productId, id);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    public void delete() throws Exception {
        Image image = Fixture.from(Image.class).gimme(ProductTemplate.VALID);
        ResponseEntity<Image> entity = this.restTemplate.postForEntity("/product/{id}/image", image, Image.class, productId);
        Long id = idFromLocation(entity);

        ResponseEntity<Void> response = this.restTemplate.exchange("/product/{id}/image/{id}", HttpMethod.DELETE, null, Void.class, productId, id);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
    }

    private Long idFromLocation(ResponseEntity<Image> entity) {
        String[] fragment = entity.getHeaders().getLocation().getPath().split("/");
        return Long.parseLong(fragment[4]);
    }

    private Long newProduct() {

        Product product = Fixture.from(Product.class).gimme(ProductTemplate.VALID);
        ResponseEntity<Product> entity = restTemplate.postForEntity("/product", product, Product.class);

        String[] fragment = entity.getHeaders().getLocation().getPath().split("/");
        return Long.parseLong(fragment[2]);
    }

}
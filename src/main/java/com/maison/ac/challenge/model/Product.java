package com.maison.ac.challenge.model;

import com.fasterxml.jackson.annotation.JsonView;
import com.maison.ac.challenge.Views;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "PRODUCT")
public class Product implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonView(Views.Basic.class)
    private Long id;

    @Column(nullable = false, unique = true)
    @JsonView(Views.Basic.class)
    private String name;

    @JsonView(Views.Basic.class)
    private String description;

    @ManyToOne
    @JsonView(Views.Resumed.class)
    @JoinColumn(name = "PARENT_FK")
    private Product parent;

    @JsonView(Views.Complete.class)
    @OneToMany(mappedBy="parent", fetch = FetchType.EAGER)
    private Set<Product> products;

    @JsonView(Views.Complete.class)
    @OneToMany(mappedBy="product", fetch = FetchType.EAGER)
    private Set<Image> images;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Product getParent() {
        return parent;
    }

    public void setParent(Product parent) {
        this.parent = parent;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }
}

package com.maison.ac.challenge;

public class Views {

    public static class Basic {
    }

    public static class Resumed extends Basic {
    }

    public static class Complete extends Basic {
    }
}

package com.maison.ac.challenge.service;

import com.maison.ac.challenge.model.Image;

import java.util.Collection;

public interface ImageService {

    void create(Image product);

    Collection<Image> list();

    void update(Image product);

    Image getById(long id);

    void delete(long id);

}

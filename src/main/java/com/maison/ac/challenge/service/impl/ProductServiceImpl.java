package com.maison.ac.challenge.service.impl;

import com.maison.ac.challenge.dao.ProductDao;
import com.maison.ac.challenge.model.Product;
import com.maison.ac.challenge.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductDao productDao;

    @Override
    public void create(Product product) {
        productDao.create(product);
    }

    @Override
    public Collection<Product> list() {
        return productDao.list();
    }

    @Override
    public void update(Product product) {
        productDao.update(product);
    }

    @Override
    public Product getById(long id) {
        return productDao.getById(id);
    }

    @Override
    public void delete(long id) {
        productDao.delete(id);
    }
}

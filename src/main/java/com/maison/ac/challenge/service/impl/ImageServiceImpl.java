package com.maison.ac.challenge.service.impl;

import com.maison.ac.challenge.dao.ImageDao;
import com.maison.ac.challenge.model.Image;
import com.maison.ac.challenge.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
@Transactional
public class ImageServiceImpl implements ImageService {
    @Autowired
    private ImageDao imageDao;

    @Override
    public void create(Image product) {
        imageDao.create(product);
    }

    @Override
    public Collection<Image> list() {
        return imageDao.list();
    }

    @Override
    public void update(Image product) {
        imageDao.update(product);
    }

    @Override
    public Image getById(long id) {
        return imageDao.getById(id);
    }

    @Override
    public void delete(long id) {
        imageDao.delete(id);
    }
}

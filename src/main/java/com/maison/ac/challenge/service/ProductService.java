package com.maison.ac.challenge.service;

import com.maison.ac.challenge.model.Product;

import java.util.Collection;

public interface ProductService {

    void create(Product product);

    Collection<Product> list();

    void update(Product product);

    Product getById(long id);

    void delete(long id);

}

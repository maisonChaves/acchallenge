package com.maison.ac.challenge;

import com.maison.ac.challenge.controller.ImageController;
import com.maison.ac.challenge.controller.ProductController;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(ProductController.class);
        register(ImageController.class);
        register(MultiPartFeature.class);
    }
}

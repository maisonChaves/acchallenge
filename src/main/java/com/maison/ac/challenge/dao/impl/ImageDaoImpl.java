package com.maison.ac.challenge.dao.impl;

import com.maison.ac.challenge.dao.ImageDao;
import com.maison.ac.challenge.model.Image;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;

@Repository
public class ImageDaoImpl implements ImageDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void create(Image image) {
        entityManager.persist(image);
    }

    @Override
    public Image getById(long id) {
        return entityManager.find(Image.class, id);
    }

    @Override
    public void update(Image image) {
        entityManager.merge(image);
    }

    @Override
    public void delete(long id) {
        Image image = getById(id);
        if (image != null) {
            entityManager.remove(image);
        }
    }

    @Override
    public Collection<Image> list() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<Image> query = cb.createQuery(Image.class);

        Root<Image> c = query.from(Image.class);
        query.select(c);

        return entityManager.createQuery(query).getResultList();
    }

}

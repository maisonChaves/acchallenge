package com.maison.ac.challenge.dao;

import com.maison.ac.challenge.model.Image;

import java.util.Collection;

public interface ImageDao {

    void create(Image image);

    void update(Image image);

    Image getById(long id);

    void delete(long id);

    Collection<Image> list();
}

package com.maison.ac.challenge.dao.impl;

import java.util.Collection;

import com.maison.ac.challenge.dao.ProductDao;
import com.maison.ac.challenge.model.Product;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

@Repository
public class ProductDaoImpl implements ProductDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void create(Product product) {
        entityManager.persist(product);
    }

    @Override
    public void update(Product product) {
        entityManager.merge(product);
    }

    @Override
    public Product getById(long id) {
        return entityManager.find(Product.class, id);
    }

    @Override
    public boolean checkExistence(Product product) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<Product> query = cb.createQuery(Product.class);

        ParameterExpression<String> parameter = cb.parameter(String.class, "name");

        Root<Product> products = query.from(Product.class);

        query.where(cb.like(products.get("name"), parameter));

        return !entityManager
                .createQuery(query)
                .setParameter(parameter, product.getName())
                .getResultList().isEmpty();
    }

    @Override
    public void delete(long id) {
        Product product = getById(id);
        if (product != null) {
            entityManager.remove(product);
        }
    }

    @Override
    public Collection<Product> list() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<Product> query = cb.createQuery(Product.class);

        Root<Product> c = query.from(Product.class);
        query.select(c);

        return entityManager.createQuery(query).getResultList();
    }

}

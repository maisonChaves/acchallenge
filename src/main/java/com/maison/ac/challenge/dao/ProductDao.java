package com.maison.ac.challenge.dao;

import java.util.Collection;

import com.maison.ac.challenge.model.Product;

public interface ProductDao {

    void create(Product product);

    void update(Product product);

    Product getById(long id);

    boolean checkExistence(Product product);

    void delete(long id);

    Collection<Product> list();
}

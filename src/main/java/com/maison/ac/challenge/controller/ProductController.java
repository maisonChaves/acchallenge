package com.maison.ac.challenge.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.maison.ac.challenge.Views;
import com.maison.ac.challenge.controller.ImageController;
import com.maison.ac.challenge.model.Product;
import com.maison.ac.challenge.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Collection;

@Component
@Path("/product")
public class ProductController {

    @Context
    private UriInfo uriInfo;

    @Autowired
    private ProductService productService;

    @Context
    private ResourceContext resourceContext;

    @GET
    @Path("/{id}")
    @JsonView(Views.Resumed.class)
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") Integer id) {

        Product product = productService.getById(id);

        return Response.ok().entity(product).build();
    }

    @GET
    @Path("/{id}/complete")
    @JsonView(Views.Complete.class)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getComplete(@PathParam("id") Integer id) {

        Product product = productService.getById(id);

        return Response.ok().entity(product).build();
    }

    @GET
    @JsonView(Views.Resumed.class)
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Product> list() {
        return productService.list();
    }

    @GET
    @Path("/{id}/products")
    @JsonView(Views.Resumed.class)
    @Produces(MediaType.APPLICATION_JSON)
    public Response listChildren(@PathParam("id") Integer id) {

        Product product = productService.getById(id);

        return Response.ok().entity(product.getProducts()).build();
    }

    @GET
    @Path("/complete")
    @JsonView(Views.Complete.class)
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Product> listComplete() {
        return productService.list();
    }

    @POST
    @JsonView(Views.Complete.class)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Product product) {

        productService.create(product);

        URI location = uriInfo.getAbsolutePathBuilder()
                .path("{id}")
                .resolveTemplate("id", product.getId())
                .build();

        return Response.created(location).build();
    }

    @PUT
    @Path("/{id}")
    @JsonView(Views.Complete.class)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") Long id, Product product) {
        product.setId(id);
        productService.update(product);

        return Response.ok().entity(product).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Integer id) {
        productService.delete(id);
        return Response.accepted().build();
    }

    @Path("/{id}/image")
    public ImageController image(@PathParam("id") Integer id) {

        Product product = productService.getById(id);

        ImageController imageController = resourceContext.getResource(ImageController.class);
        imageController.setProduct(product);
        return imageController;
    }
}

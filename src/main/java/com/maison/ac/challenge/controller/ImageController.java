package com.maison.ac.challenge.controller;

import com.maison.ac.challenge.model.Image;
import com.maison.ac.challenge.model.Product;
import com.maison.ac.challenge.service.ImageService;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import java.io.InputStream;
import java.net.URI;
import java.util.Collection;

public class ImageController {

    private Product product;

    @Autowired
    private ImageService imageService;

    @Context
    private UriInfo uriInfo;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Image> list() {
        return product.getImages();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") Integer id) {

        Image image = imageService.getById(id);

        return Response.ok().entity(image).build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response create(@FormDataParam("file") InputStream file,
                           @FormDataParam("file") FormDataContentDisposition fileDisposition,
                           @FormDataParam("file") FormDataBodyPart body) {

        if(product == null) {
            return Response.status(Status.BAD_REQUEST).build();
        }

        Image image = new Image();
        image.setType(body.getMediaType().toString());
        image.setProduct(product);

        imageService.create(image);

        URI location = uriInfo.getAbsolutePathBuilder()
                .path("{id}")
                .resolveTemplate("id", image.getId())
                .build();

        return Response.created(location).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createJson(Image image) {

        image.setProduct(product);

        imageService.create(image);

        URI location = uriInfo.getAbsolutePathBuilder()
                .path("{id}")
                .resolveTemplate("id", image.getId())
                .build();

        return Response.created(location).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") Long id, Image image) {

        image.setId(id);
        image.setProduct(product);

        imageService.update(image);

        return Response.ok().entity(image).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Integer id) {
        imageService.delete(id);
        return Response.accepted().build();
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}

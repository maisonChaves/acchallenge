# AC Challenge #

Simple Rest API for products and images.

## How to compile and run the application with an example for each call. 

Use `mvn clean package` to compile the application. 
To run use `mvn spring-boot:run`.

A JSON documentation for API can be seen on file **AC Challenge.json** or in [Postman](https://documenter.getpostman.com/view/10981/ac-challenge/6Z6rqFG). 

### Create Product
---------

* **URL** http://localhost:8080/product

* **Method** `POST`

* **Headers** 

    * **Content-Type** application/json

* **Request** `{
        "name": "Celular Moto Z",
        "description": "Celular Motorola by Lenovo"
    }`

* **Response** 

    * **Code** 201

* **Sample Call**

```curl 
curl --request POST \
  --url http://localhost:8080/product \
  --header 'content-type: application/json' \
  --data '{
    "name": "Celular Moto Z",
    "description": "Celular Motorola by Lenovo"
}'
```

### Update Product
---------

* **URL** http://localhost:8080/product/:productId

* **Method** `PUT`

* **Headers** 

    * **Content-Type** application/json

* **Request** `{
        "name": "Celular Moto Z",
        "description": "Celular Motorola by Lenovo"
    }`

* **Response** 

    * **Code** 200
    * **Content** `{
    "id": 1,
    "name": "Celular Moto X",
    "description": "Celualar Motorola by Lenovo (2015)",
    "products": null,
    "images": null
}`

* **Sample Call**

```curl 
curl --request PUT \
  --url http://localhost:8080/product/1 \
  --header 'content-type: application/json' \
  --data '{
    "name": "Celular Moto X",
    "description": "Celualar Motorola by Lenovo (2015)"
}'
```

###  Delete Product
---------

* **URL** http://localhost:8080/product/:productId

* **Method** `DELETE`

* **Response** 

    * **Code** 202

* **Sample Call**

```curl 
curl --request DELETE \
  --url http://localhost:8080/product/1
```

### List Product
---------

* **URL** http://localhost:8080/product

* **Method** `GET`

* **Response** 

    * **Code** 200
    * **Content** `[{
    "id": 1,
    "name": "Celular Moto X",
    "description": "Celualar Motorola by Lenovo",
    "parent": null
  }]`

* **Sample Call**

```curl 
curl --request GET \
  --url http://localhost:8080/product
```

### List Product Complete
---------

* **URL** http://localhost:8080/product/complete

* **Method** `GET`

* **Response** 

    * **Code** 200
	* **Content** `[{
    "id": 1,
    "name": "Celular Moto X",
    "description": "Celualar Motorola by Lenovo (2015)",
    "products": [
      {
        "id": 194,
        "name": "Fone Moto X",
        "description": "Fone de Ouvidos para Celualar Motorola by Lenovo",
        "products": [],
        "images": []
      },
      {
        "id": 259,
        "name": "Fone Moto E",
        "description": "Fone de Ouvidos para Celualar Motorola by Lenovo",
        "products": [],
        "images": []
      }
    ],
    "images": [
      {
        "id": 33,
        "type": "image/jpeg"
      },
      {
        "id": 65,
        "type": "image/jpeg"
      }
    ]
  }]`

* **Sample Call**

```curl 
curl --request GET \
  --url http://localhost:8080/product/complete
```

### Get Product
---------

* **URL** http://localhost:8080/product/:productId

* **Method** `GET`

* **Response** 

    * **Code** 200
    * **Content** `{
  "id": 1,
  "name": "Celular Moto X",
  "description": "Celualar Motorola by Lenovo (2015)",
  "parent": null
}`

* **Sample Call**

```curl 
curl --request GET \
  --url http://localhost:8080/product/1
```

### Get Product Complete
---------

* **URL** http://localhost:8080/product/complete/:productId

* **Method** `GET`

* **Response** 

    * **Code** 200
    * **Content** `{
  "id": 1,
  "name": "Celular Moto X",
  "description": "Celualar Motorola by Lenovo (2015)",
  "products": [
    {
      "id": 194,
      "name": "Fone Moto X",
      "description": "Fone de Ouvidos para Celualar Motorola by Lenovo",
      "products": [],
      "images": []
    },
    {
      "id": 259,
      "name": "Fone Moto E",
      "description": "Fone de Ouvidos para Celualar Motorola by Lenovo",
      "products": [],
      "images": []
    }
  ],
  "images": [
    {
      "id": 65,
      "type": "image/jpeg"
    },
    {
      "id": 33,
      "type": "image/jpeg"
    }
  ]
}`

* **Sample Call**

```curl 
curl --request GET \
  --url http://localhost:8080/product/1/complete
```

### Create Child Product
---------

* **URL** http://localhost:8080/product

* **Method** `POST`

* **Headers** 

    * **Content-Type** application/json

* **Request** `{
    "name": "Fone Moto Z",
    "description": "Fone de Ouvidos para Celualar Motorola by Lenovo",
    "parent": {
    	"id": 292
    }
}`

* **Response** 

    * **Code** 201

* **Sample Call**

```curl 
curl --request POST \
  --url http://localhost:8080/product \
  --header 'content-type: application/json' \
  --data '{
    "name": "Fone Moto Z",
    "description": "Fone de Ouvidos para Celualar Motorola by Lenovo",
    "parent": {
    	"id": 292
    }
}'
```

### List Child Product
---------

* **URL** http://localhost:8080/product/:productId/products

* **Method** `GET`

* **Response** 

    * **Code** 200
    * **Content** `[{
    "id": 1,
    "name": "Celular Moto X",
    "description": "Celualar Motorola by Lenovo",
    "parent": null
  }]`

* **Sample Call**

```curl 
curl --request GET \
  --url http://localhost:8080/product/1/products
```

### Create Product Image
---------

* **URL** http://localhost:8080/product/:productId/image

* **Method** `POST`

* **Headers** 

    * **Content-Type** application/json

* **Request** `{
	"type":"image/jpeg"
}`

* **Response** 

    * **Code** 201

* **Sample Call**

```curl 
curl --request POST \
  --url http://localhost:8080/product/1/image \
  --header 'content-type: application/json' \
  --data '{
	"type":"image/jpeg"
}'
```

### Update Product Image
---------

* **URL** http://localhost:8080/product/:productId/image/:imageId

* **Method** `PUT`

* **Headers** 

    * **Content-Type** application/json

* **Request** `{
	"type":"image/gif"
}`

* **Response** 

    * **Code** 200
    * **Content** `{
  "id": 1,
  "type": "image/gif"
}`

* **Sample Call**

```curl 
curl --request PUT \
  --url http://localhost:8080/product/1/image/1 \
  --header 'content-type: application/json' \
  --data '{
	"type":"image/gif"
}'
```

###  Delete Product
---------

* **URL** http://localhost:8080/product/:productId/image/:imageId

* **Method** `DELETE`

* **Response** 

    * **Code** 202

* **Sample Call**

```curl 
curl --request DELETE \
  --url http://localhost:8080/product/1/image/1
```

### List Product
---------

* **URL** http://localhost:8080/product/:productId/image

* **Method** `GET`

* **Response** 

    * **Code** 200
    * **Content** `[
  {
    "id": 65,
    "type": "image/jpeg"
  },
  {
    "id": 97,
    "type": "image/jpeg"
  },
  {
    "id": 98,
    "type": "image/gif"
  }
]`

* **Sample Call**

```curl 
curl --request GET \
  --url http://localhost:8080/product/1/image
```

### Upload Product Image
---------

* **URL** http://localhost:8080/product/:productId/image

* **Method** `POST`

* **Request** image.jpg

* **Response** 

    * **Code** 201

* **Sample Call**

```curl 
curl --request POST \
  --url http://localhost:8080/product/1/image \
  -F "file=@/home/user/Desktop/image.jpg"
```

## How to run the suite of automated tests. 

To run test just use `mvn test`

## Mention anything that was asked but not delivered and why, and any additional comments.

* I've some issue with upload files with spring boot and jersey. At this point a could not deliver this feature fully developed.